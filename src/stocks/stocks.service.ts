import { Injectable } from '@nestjs/common';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { Stock } from './entities/stock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class StocksService {
  constructor(
    @InjectRepository(Stock)
    private stocksRepository: Repository<Stock>,
  ) {}

  create(createStockDto: CreateStockDto): Promise<Stock> {
    return this.stocksRepository.save(createStockDto);
  }

  findAll() {
    return this.stocksRepository.find();
  }

  findOne(id: number) {
    return this.stocksRepository.findOneBy({ id: id });
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    await this.stocksRepository.update(id, updateStockDto);
    const stock = await this.stocksRepository.findOneBy({ id });
    return stock;
  }

  async remove(id: number) {
    const deleteStock = await this.stocksRepository.findOneBy({ id });
    return this.stocksRepository.remove(deleteStock);
  }
}
