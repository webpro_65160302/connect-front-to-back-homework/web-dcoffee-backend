import { IsNotEmpty, Length, IsNumber } from 'class-validator';

export class CreateStockDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNumber()
  qty: number;

  @IsNumber()
  priceUnit: number;
}
